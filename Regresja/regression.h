#ifndef REGRESSION_H
#define REGRESSION_H
#include <QFile>
#include <QTextStream>
#include <QSet>
#include <QPointF>


class Regression {

private:

    /*
     * Konstruktor domyslny
     */
    Regression();

public:

    /*
     * Metoda statyczna read_points_from_file
     * Wczytuje plik tekstowy zawierajacy w kolejnych wiersza pary X Y
     * Zwraca zbior wczytanych punktow
     */
    QList<QPointF> read_points_from_file(const QString& filename);

    /*
     * Metoda statyczna linear_regression
     * Przyjmuje zbior punktow doswiadczalnych
     * Zwraca wspolczynniki a oraz b prostej regresjii
     * Wykorzystywana jest metoda najmniejszych kwadratow (bledow)
     */
    QPair<qreal,qreal> linear_regression(const QList<QPointF>& experimental_points);

};

#endif // REGRESSION_H
