#include "regression.h"


Regression::Regression() {

}

QList<QPointF> Regression::read_points_from_file(const QString& filename) {

    QList<QPointF> points;

    QFile file(filename);
    if(file.open(QIODevice::ReadOnly | QIODevice::Text)) {

        QTextStream in_stream(&file);

        while(!in_stream.atEnd()) {

            QStringList coordinates = in_stream.readLine().split(" ");
            points.push_back(QPointF(coordinates[0].toDouble(), coordinates[1].toDouble()));
        }
    }
    else {
        qDebug("Cannot open file.");
    }

    return points;
}

QPair<qreal,qreal> Regression::linear_regression(const QList<QPointF>& experimental_points) {

    QPair<qreal,qreal> line_coefficients;

    const int number = experimental_points.count();
    qreal xy_sum = 0.0, x_sum = 0.0, y_sum = 0.0, x2_sum = 0.0;

    foreach(const QPointF& point, experimental_points) {

        xy_sum += point.x() * point.y();
        x_sum += point.x();
        y_sum += point.y();
        x2_sum += point.x() * point.x();
    }

    line_coefficients.first = (xy_sum - x_sum * y_sum / number) / (x2_sum - x_sum * x_sum / number);
    line_coefficients.second = (y_sum - line_coefficients.first * x_sum) / number;

    return line_coefficients;
}
